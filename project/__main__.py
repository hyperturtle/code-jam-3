# import os
# import json
# import inspect
# import traceback

import random
import time
# import textwrap
from io import BytesIO  # , StringIO
# from contextlib import redirect_stdout

# from nested_lookup import nested_lookup
import numpy as np
import discord
from discord.ext import commands
import aiohttp
from PIL import Image, ImageChops  # ImageFont, ImageDraw
import requests
from project import prefixes, confg, mz, rg


from vapory import *

class Bot(commands.Bot):
    def __init__(self, **kwargs):
        super().__init__(command_prefix=commands.when_mentioned_or('.'), **kwargs)
        self.prefix_data = prefixes.read_prefixes()
        self.cached_screencaps = {}
        for cog in confg.cogs:
            try:
                self.load_extension(cog)
            except Exception as e:
                print(f'Could not load extension {cog} due to {e.__class__.__name__}: {e}')

    async def on_ready(self):
        self.session = aiohttp.ClientSession(loop=bot.loop)
        print('Logged on as {0} (ID: {0.id}) '.format(self.user))
        print(f'Logged in {self.user.name} ID:{self.user.id} '+
              f'Connected to {len(self.guilds)} servers | {self.guilds} '+
              f'Connected to {len(set(self.get_all_members()))} users')
        mems = list(self.get_all_members())
        modlist = [95872159741644800, 109040264529608704, 140605665772175361,
                   145475247074705408, 125435062127820800, 188735535169273858,
                   172395097705414656, 98195144192331776, 223168485419778060]
        mm = []
        for i in modlist:
            x = await self.get_user_info(i)
            mm.append(x)
        self.prof_url = [(x.avatar_url_as(static_format="png", size=64),
                          [sum(map(int, str(x.id))), sum(map(ord, x.name))]) for x in mm+mems]
        self.randlist = []
        for i, j in bot.prof_url:
            im = Image.open(requests.get(i, stream=True).raw)
            bgr = Image.new(im.mode, im.size, im.getpixel((0, 0)))
            diff = ImageChops.difference(im, bgr)
            diff = ImageChops.add(diff, diff, 2.0, -100)
            bbox = diff.getbbox()
            bg = im.crop(bbox)
            # print(f'new_size {bg.size} - cropped')
            if bg.width > 40:
                b = bg.width/40
                bg = bg.resize((40, round(bg.height/b)))
            self.randlist.append((bg, j))


bot = Bot()
bot.remove_command('help')


@bot.command()
async def rand(ctx):
    e = discord.Embed()
    randpic = random.choice(bot.prof_url)
    print(randpic)
    e.set_image(url=randpic)
    await ctx.send(embed=e)
    im = Image.open(requests.get(randpic, stream=True).raw)
    print(im)
    im = im.convert('RGBA')
    pic = BytesIO()
    im2 = im
    im2.thumbnail((200, 200))
    im2.save(pic, 'PNG')
    pic.seek(0)
    e = discord.Embed()
    giff = [discord.File(pic, 'test.png')]
    e.set_image(url='attachment://test.png')
    await ctx.send(files=giff, embed=e)
    bgr = Image.new(im.mode, im.size, im.getpixel((0, 0)))
    diff = ImageChops.difference(im, bgr)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    bg = im.crop(bbox)
    print(f'new_size {bg.size} - cropped')
    if bg.width > 200:
        b = bg.width/200
        bg = bg.resize((200, round(bg.height/b)))
    pic = BytesIO()
    bg.save(pic, 'PNG')
    pic.seek(0)
    e = discord.Embed()
    giff = [discord.File(pic, 'test.png')]
    e.set_image(url='attachment://test.png')
    await ctx.send(files=giff, embed=e)


@bot.command()
async def blank(ctx):
    blank = Image.new("RGBA", (200, 200))
    e = discord.Embed()
    pic = BytesIO()
    blank.save(pic, "PNG")
    pic.seek(0)
    giff = [discord.File(pic, 'test.png')]
    e.set_image(url='attachment://test.png')
    randpic = random.choice(bot.prof_url)
    print(randpic)
    im = Image.open(requests.get(randpic, stream=True).raw)
    print(im)
    await ctx.send(files=giff, embed=e, delete_after=2)
    pic = BytesIO()
    bgr = Image.new(im.mode, im.size, im.getpixel((0, 0)))
    diff = ImageChops.difference(im, bgr)
    diff = ImageChops.add(diff, diff, 2.0, -100)
    bbox = diff.getbbox()
    bg = im.crop(bbox)
    print(f'new_size {bg.size} - cropped')
    if bg.width > 40:
        b = bg.width/40
        bg = bg.resize((40, round(bg.height/b)))
        bgcv = [bg.convert('RGBA'), bg.convert("L"), bg.convert("1")]
    for _ in range(10):
        pic = BytesIO()
        blankn = blank
        bg = random.choice(bgcv)
        blankn.paste(bg, (random.randrange(0, 160, 40), random.randrange(0, 160, 40)))  # ,bg
        time.sleep(2)
        blankn.save(pic, 'PNG')
        pic.seek(0)
        e = discord.Embed()
        giff = [discord.File(pic, 'test.png')]
        e.set_image(url='attachment://test.png')
        await ctx.send(files=giff, embed=e, delete_after=2)
    pic = BytesIO()
    blankn.save(pic, "PNG")
    pic.seek(0)
    e = discord.Embed()
    giff = [discord.File(pic, 'test.png')]
    e.set_image(url='attachment://test.png')
    await ctx.send(files=giff, embed=e)


@bot.command()
async def grid(ctx, num: int = 3):
    blank = Image.new("RGBA", (num*40, num*40))
    cv = ["1", "RGBA", "L"]
    ans = []
    for i in range(0, num*40, 40):
        for j in range(0, num*40, 40):
            image, anw = random.choice(bot.randlist)
            blank.paste(image.convert(random.choice(cv)), (i, j))
            pic = BytesIO()
            blank.save(pic, 'PNG')
            pic.seek(0)
            e = discord.Embed()
            giff = [discord.File(pic, 'test.png')]
            e.set_image(url='attachment://test.png')
            ans.append(random.choice(anw))
    e.set_footer(text=ans)
    await ctx.send(files=giff, embed=e)


@bot.command()
async def maze(ctx, num: int = 10):
    e = discord.Embed()
    if num > 30:
        num = 30
    e.add_field(name="maze", value=f"```{mz.Maze(num,num).__str__()}```")
    await ctx.send(embed=e)


@bot.command()
async def rogue(ctx, row: int = 30, fill: float = 0.41):
    a = rg.rogue(row, row, fill)
    c = a.gen_map()
    x, y = a.center_pt
    c[x][y] = 9
    valid = []
    for i, x in enumerate(c):
        for j, y in enumerate(x):
            if y != 0:
                valid.append((i, j))
    await ctx.send(f"```{a.print_grid(c)}```")
    x = [1, 0, 0]
    y = [0, 1, 0]
    z = [0, 0, 1]
    camera = Camera('location', [0, 40, 0],
                'up',       [ 0.0, -1, 0.0],
                'right',    [ 0, 0.0, 0.0],
                'rotate',   [ 0, 0, 0],
                'look_at',  [ 0, 0, 0])
    light1 = LightSource([-30, 70, 10],
                     'color', 'White')
    light2 = LightSource([20, 60, 20],
                     'color', 'Yellow')             
    boxes = []
    for i,row in enumerate(c):
        for j,colm in enumerate(row):
            if colm > 1:
                box = Box([i-a.center_pt[0], 0, -j-a.center_pt[1]],
                           [i-a.center_pt[0]+1, 10, -j-a.center_pt[1]-1],
                               Texture(
                                  'T_Wood27',
                               Finish('phong', 1,
                                     'phong_size', 300,
                                      'reflection', 0.15)))
                if colm == 9:
                     sphere = Sphere([i-a.center_pt[0], 11, -j-a.center_pt[1]],
                                      1.0,
                                      Texture(
                                              'T_Glass1',
                                      Pigment('color', 'green', 0.9,
                                              'filter', 0.85),
                                      Finish('phong', 1,
                                             'phong_size', 300,
                                             'reflection', 0.15)))
            else:
                box = Box([i-a.center_pt[0], 0, -j-a.center_pt[1]],
                          [i-a.center_pt[0]+1, 5, -j-a.center_pt[1]-1],
                            Texture(
                             'T_Stone1',
                             Finish('phong', 1,
                                           'phong_size', 200,
                                            'reflection', 0.5)))
            boxes.append(box)
    txture = ['T_Wood20', 'T_Wood21', 'T_Stone1', 'T_Stone2', 'T_Stone3']
    plane = Plane(y,
              0,
              Texture(
                  random.choice(txture),
                  Pigment('octaves', 4,
                          'rotate', [i * 45 for i in z]),
                  Finish('reflection', 0.10)))
                
    scene = Scene(camera,
              objects=[light1, light2, *boxes, plane, sphere],
              included=["colors.inc", "stones.inc", "woods.inc", "glass.inc"],
              global_settings=["assumed_gamma 2.2"])
              
    scn = Image.fromarray(scene.render(width=64, height=64))  
    pic = BytesIO()
    scn.save(pic, 'PNG')
    pic.seek(0)
    e = discord.Embed()
    giff = [discord.File(pic, 'test.png')]
    e.set_image(url='attachment://test.png')
    await ctx.send(files=giff, embed=e)
    
    
bot.run(confg.token)
